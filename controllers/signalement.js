const Joi = require('joi')

const signalementModel = require('../models/signalement'),
  statutModel = require('../models/statut'),
  etatModel = require('../models/etat'),
  brigadeModel = require('../models/brigade'),
  animalModel = require('../models/animal'),
  { signalementSchema, signalementUpdateSchema } = require('../schemas/signalement'),
  { sendMail } = require('../nodemailer'),
  response = require('../response')

// On pourrait ajouter une vérification d'identité pour vérifier que les signalements ne puissent pas être obtenu par n'importe qui
// => Token ou session
// On pourrait ajouter un affichage particulier au brigades pour que les brigades ne voit que les signalements qui leur ont été assignées
// => correspondance entre identité du client et table d'assignement
function show(req, res){
  const { statut_id } = req.query
  // on teste si on nous demande une liste par statut ou toute la liste
  if(!statut_id){
    signalementModel.all().then( signalementList => {
      response.success(res, signalementList)
    }).catch( err => {
      console.log(err)
      response.error(res, 503, "oups, interal error")
    })
  } else {
    // vérification syntaxique
    const queryError = Joi.validate(statut_id, Joi.number().integer())
    if(queryError.error) {
      return response.error(res, 403, "La requête n'a pas le bon format")
    }
    statutModel.byId(statut_id).then( results => {
      // vérification sémantique, ce qu'on nous demande est il possible ?
      if(results.length < 1){
        return response.error(res, 400, "Ce statut n'existe pas")
      }
      // Pas d'erreur, on lance la requête
      signalementModel.by({statut_id}).then( signalementList => {
        response.success(res, signalementList)
      }).catch( err => {
        console.log(err)
        response.error(res, 503, "oups, internal error")
      })
    })
  }   
}

// Il faudrait vérifier la validité de l'adresse mail de l'alerteur ainsi que son format ce qui n'est pas fait actuellement
function add(req, res){
  const signalement = req.body
  // vérification syntaxique
  const signalementError = Joi.validate(signalement, signalementSchema.required())
  if(signalementError.error) {
    console.log(signalementError.error)
    return response.error(res, 403, "Ce signalement n'a pas le bon format")
  }
  // verification sémantique
  const etat = etatModel.byId(signalement.etat_id)
  const animal = animalModel.byId(signalement.animal_id)
  Promise.all([etat, animal]).then( results => {
    results.forEach( result => {
      if(results.length < 1){
        throw new Error("Cet état ou cet animal n'existe pas")
      }
    })
    // format
    signalement.creneau = signalement.creneau.map( hour => hour + 'H')
    signalement.creneau = signalement.creneau.join(' - ')
    signalement.statut_id = 1
    const date = new Date(signalement.date)
    signalement.date = date.toISOString()
    // Pas d'erreur, on lance la requête
    signalementModel.add(signalement).then( id => {
      sendMail('Nouveau Signalement', 'Nouveau signalement au ' + 
        signalement.adresse + 
        ' entre ' + 
        signalement.creneau + 
        ' le ' + 
        date.getDate() + 
        '/' + 
        (date.getMonth() + 1 < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1))
    )
      response.success(res, "ajout du signalement à l'id: " + id)
    })
    
  }).catch( err => {
    console.log(err)
    if(err.message === "Cet état ou cet animal n'existe pas"){
      return response.error(res, 403, err)
    }
    return response.error(res, 503, "oups, internal error")
  })
}

// On pourrait ajouter une sécurité pour que l'update ne soit requêter que par la brigade concernée
// ou par un rôle supérieur ce qui implique au moins deux nouvelle table: user et rôle
function update(req, res){
  const { signalement_id } = req.params
  const signalementUpdate = req.body
  // vérification syntaxique
  const signalementError = Joi.validate(signalementUpdate, signalementUpdateSchema)
  if(signalementError.error || !signalement_id) {
    console.log(signalementError.error)
    return response.error(res, 403, "La requête n'a pas le bon format")
  }
  // vérification sémantique
  const signalement = signalementModel.by({["signalements.id"]: signalement_id})
  const etat = signalement.etat_id ? etatModel.byId(signalement.etat_id) : Promise.resolve(false)
  const animal = signalement.animal_id ? animalModel.byId(signalement.animal_id) : Promise.resolve(false)
  const statut = signalement.statut_id ? statutModel.byId(signalement.statut_id) : Promise.resolve(false)
  const brigade = signalement.brigade_id ? brigadeModel.byId(signalement.brigade_id) : Promise.resolve(false)

  return Promise.all([signalement, etat, animal, statut, brigade]).then( results => {
    const signalement = signalementUpdate
    const signalementOriginal = results[0][0]
    results.forEach( result => {
      if(results === false) return
      if(results.length < 1){
        throw new Error("Certaines données n'existent pas")
      }
    })
    // format
    if(signalement.creneau){
      signalement.creneau = signalement.creneau.map( hour => hour + 'H')
      signalement.creneau = signalement.creneau.join(' - ')
    }
    if(signalement.brigade_id && signalementOriginal.statut_id < 2){
      signalement.statut_id = 2
    }
    if(signalement.date){
      const date = new Date(signalement.date)
      signalement.date = date.toISOString()
    }
    // requête
    return signalementModel.update(signalement_id, signalement).then( () => {
      if(signalement.statut_id && signalementOriginal.statut_id < 3 && signalement.statut_id >= 3){
        if(signalement.statut_id == 3){
          sendMail('no-reply', 'Votre signalement pour un ' +
            signalementOriginal.animal + 
            " au " + 
            signalementOriginal.adresse +
            " nous a permis de sauver l'animal, merci à vous.",
            (signalement.alerteur ? signalement.alerteur : signalementOriginal.alerteur)  
          )
        } else {
          sendMail('no-reply', 'Votre signalement pour un ' +
            signalementOriginal.animal + 
            " au " + 
            signalementOriginal.adresse +
            " ne nous a pas permis de récupérer l'animal." +
            " Nous vous remercions, restez vigilant.",
            (signalement.alerteur ? signalement.alerteur : signalementOriginal.alerteur)  
          )
        }
      }
      response.success(res, "signalement mis à jour !")
    }).catch(err => {
      console.log(err)
      response.error(res, 503, "oups, internal error")
    })
  })
}

module.exports = {
  add,
  show,
  update
}