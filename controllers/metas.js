const statutModel = require('../models/statut'),
  brigadeModel = require('../models/brigade'),
  etatModel = require('../models/etat'),
  animalModel = require('../models/animal'),
  response = require('../response')

function showStatuts(req, res){
  statutModel.all().then( results => {
    response.success(res, results)
  }).catch( err => {
    console.log(err)
    return response.error(res, 503, "oups, internal error")
  })
}


function showBrigades(req, res){
  brigadeModel.all().then( results => {
    response.success(res, results)
  }).catch( err => {
    console.log(err)
    return response.error(res, 503, "oups, internal error")
  })
}

function showEtats(req, res){
  etatModel.all().then( results => {
    response.success(res, results)
  }).catch( err => {
    console.log(err)
    return response.error(res, 503, "oups, internal error")
  })
}


function showAnimaux(req, res){
  animalModel.all().then( results => {
    response.success(res, results)
  }).catch( err => {
    console.log(err)
    return response.error(res, 503, "oups, internal error")
  })
}


module.exports = {
  showStatuts,
  showBrigades,
  showAnimaux,
  showEtats
}