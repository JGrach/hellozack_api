const express = require('express'),
  app = express(),
  cors = require('cors')
const { api: { port } } = require('./config.json')[process.env.NODE_ENV],
  signalementCtrl = require('./controllers/signalement'),
  metaCtrl = require('./controllers/metas')

app.use(express.json())
app.use(cors())

// S'il y avait eu un peu plus de routes, je les aurait splité dans d'autres fichiers

app.get('/signalements', signalementCtrl.show)
app.get('/statuts', metaCtrl.showStatuts)
app.get('/brigades', metaCtrl.showBrigades)
app.get('/etats', metaCtrl.showEtats)
app.get('/animaux', metaCtrl.showAnimaux)

app.post('/signalement', signalementCtrl.add)
app.put('/signalement/:signalement_id', signalementCtrl.update)

app.listen(process.env.PORT || port)


