const Joi = require('joi')

// A priori on peut envisager de factoriser les schémas

const signalementSchema = Joi.object().keys({
  date: Joi.date().max('now').iso().required(),
  creneau: Joi.array().items(Joi.number().integer()).length(2).required(),
  alerteur: Joi.string().min(3).max(50).required(),
  couleur: Joi.string().min(3).max(50).required(),
  adresse: Joi.string().min(3).max(100).required(),
  collier: Joi.boolean().required(),
  animal_id: Joi.number().integer().required(),
  etat_id: Joi.number().integer().required(),
})

const signalementUpdateSchema = Joi.object().keys({
  date: Joi.date().max('now').iso(),
  creneau: Joi.array().items(Joi.number().integer()).length(2),
  alerteur: Joi.string().min(3).max(50),
  couleur: Joi.string().min(3).max(50),
  adresse: Joi.string().min(3).max(100),
  collier: Joi.boolean(),
  animal_id: Joi.number().integer(),
  etat_id: Joi.number().integer(),
  brigade_id: Joi.number().integer(),
  statut_id: Joi.number().integer(),
})

module.exports = {
  signalementSchema,
  signalementUpdateSchema
}