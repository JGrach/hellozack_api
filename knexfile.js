
if(!process.env.NODE_ENV) process.env.NODE_ENV = 'development'
if(process.env.NODE_ENV != "production") 
  require('dotenv').config({path: './bdd.env'})
const { database } = require('./config.json')[process.env.NODE_ENV]

module.exports = {
  development: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    },
    migrations: {
      directory: __dirname + '/models/migrations',
    },
    seeds: {
      directory: __dirname + '/models/seeds/development',
    },
  },
  stage: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    },
    migrations: {
      directory: __dirname + '/models/migrations',
    },
    seeds: {
      directory: __dirname + '/models/seeds/development',
    },
  },
  docker: {
    client: 'pg',
    connection: {
      host: database.host,
      port: database.port,
      user: process.env.POSTGRES_USER,
      password: process.env.POSTGRES_PASSWORD,
      database: process.env.POSTGRES_DB,
    },
    migrations: {
      directory: __dirname + '/models/migrations',
    },
    seeds: {
      directory: __dirname + '/models/seeds/development',
    },
  },
}
