const knex = require('./knex')

function all(){
  return knex('brigades')
    .select()
}

function byId(id){
  return knex('brigades')
    .select()
    .where({id})
}

module.exports = {
  all,
  byId,
}
