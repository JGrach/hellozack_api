const knex = require('./knex')

// au vu des models metas, on devrait pouvoir les factoriser

function all(){
  return knex('animaux')
    .select()
}

function byId(id){
  return knex('animaux')
    .select()
    .where({id})
}

module.exports = {
  all,
  byId,
}
