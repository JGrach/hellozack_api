const knex = require('./knex')

function all(){
  return knex('etats')
    .select()
}

function byId(id){
  return knex('etats')
    .select()
    .where({id})
}

module.exports = {
  all,
  byId,
}
