
exports.up = function(knex, Promise) {
  return knex.schema.createTable('signalements', table => {
    table.increments()
    table.timestamps(false, true)
    table.date('date')
    table.string('creneau').notNullable()
    table.string('alerteur').notNullable()
    table.string('adresse').notNullable()
    table.string('couleur').notNullable()
    table.boolean('collier').notNullable().defaultTo(false)

    table
      .integer('animal_id')
      .unsigned()
      .notNullable()
    table
      .foreign('animal_id')
      .references('animaux.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
    
    table
      .integer('etat_id')
      .unsigned()
      .notNullable()
    table
      .foreign('etat_id')
      .references('etats.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')

    table
      .integer('statut_id')
      .unsigned()
      .notNullable()
    table
      .foreign('statut_id')
      .references('statuts.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')

    table
      .integer('brigade_id')
      .unsigned()
    table
      .foreign('brigade_id')
      .references('brigades.id')
      .onUpdate('CASCADE')
      .onDelete('CASCADE')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('signalements')
}
