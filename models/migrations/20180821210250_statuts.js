
exports.up = function(knex, Promise) {
  return knex.schema.createTable('statuts', table => {
    table.increments()
    table.string('nom').notNullable()
    table.string('description')
  })
}

exports.down = (knex, Promise) => {
  return knex.schema.dropTable('statuts')
}