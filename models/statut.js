const knex = require('./knex')

function all(){
  return knex('statuts')
    .select()
}

function byId(id){
  return knex('statuts')
    .select()
    .where({id})
}

module.exports = {
  all,
  byId,
}
