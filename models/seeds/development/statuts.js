
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('statuts').del()
    .then(function () {
      // Inserts seed entries
      return knex('statuts').insert([
        {id: 1, nom: 'signalé'},
        {id: 2, nom: 'assigné'},
        {id: 3, nom: 'sauvé'},
        {id: 4, nom: 'échec'},
        {id: 5, nom: 'annulé'},
      ]);
    });
};
