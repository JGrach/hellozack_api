
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('etats').del()
    .then(function () {
      // Inserts seed entries
      return knex('etats').insert([
        {id: 1, nom: 'très faible'},
        {id: 2, nom: 'faible'},
        {id: 3, nom: 'moyen'},
        {id: 4, nom: 'bon'},
      ]);
    });
};
