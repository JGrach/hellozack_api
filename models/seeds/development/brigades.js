
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('brigades').del()
    .then(function () {
      // Inserts seed entries
      return knex('brigades').insert([
        {id: 1, nom: 'Les quatres fantastiques'},
        {id: 2, nom: 'La communauté de l\'anneau'},
        {id: 3, nom: 'Hubert Bonisseur de La Bath'},
        {id: 4, nom: 'X-men'},
        {id: 5, nom: 'Les tortues ninjas'},
      ]);
    });
};
