
exports.seed = function(knex, Promise) {
  // Deletes ALL existing entries
  return knex('animaux').del()
    .then(function () {
      // Inserts seed entries
      return knex('animaux').insert([
        {id: 1, nom: 'chat'},
        {id: 2, nom: 'chien'},
        {id: 3, nom: 'perroquet'},
        {id: 4, nom: 'lapin'},
      ]);
    });
};
