const knex = require('./knex')

function signalements() {
  return knex('signalements')
}

function all(){
  return signalements().select(
    'signalements.*',
    'brigades.nom as brigade',
    "etats.nom as etat",
    "animaux.nom as animal",
    "statuts.nom as statut"
  )
  .join("etats", "etats.id", "signalements.etat_id")
  .join("statuts", "statuts.id", "signalements.statut_id")
  .join("animaux", "animaux.id", "signalements.animal_id")
  .leftJoin("brigades", "brigades.id", "signalements.brigade_id")
}

function by(constraint){
  return signalements()
    .select(
      'signalements.*',
      'brigades.nom as brigade',
      "etats.nom as etat",
      "animaux.nom as animal",
      "statuts.nom as statut"
    )
    .join("etats", "etats.id", "signalements.etat_id")
    .join("statuts", "statuts.id", "signalements.statut_id")
    .join("animaux", "animaux.id", "signalements.animal_id")
    .leftJoin("brigades", "brigades.id", "signalements.brigade_id")
    .where(constraint)
}

function add(signalement){
  return signalements()
    .insert(signalement)
    .returning('id')
    .into('signalements')
}

function update(id, updateData){
  return signalements()
    .where({id})
    .update(updateData)
}

module.exports = {
  all,
  by,
  add,
  update
}


