FROM node:9.11.1-stretch

run npm install -g knex

RUN mkdir /app
WORKDIR /app
ADD . ./
RUN npm install

ENTRYPOINT ["npm", "run"]
CMD ["start"]
