// On pourrait envisager d'ajouter ces méthodes de formatage en prototype de res afin de pouvoir utiliser this plutôt qu'un argument res.

// To send success response
function success(res, data) {
  const response = {
    error: null,
    data: data.rows ? data.rows : data,
  }
  res.status(200).json(response)
}

// To send error response
function error(res, code, err) {
  err = err.message ? err.message : err
  if (isNaN(code)) code = 500
  const response = {
    error: err,
    data: null,
  }
  res.status(code).json(response)
}

module.exports = {
  success,
  error,
}
