if(!process.env.NODE_ENV) process.env.NODE_ENV = 'development'
if(process.env.NODE_ENV != "production") require('dotenv').config({path: './nodemailer.env'})

const nodemailer = require('nodemailer')
const { api: { mailResponsable } } = require('./config.json')[process.env.NODE_ENV]

const transporter = nodemailer.createTransport({
  service: process.env.NODEMAILER_SERVICE,
  auth: {
      user: process.env.NODEMAILER_MAIL,
      pass: process.env.NODEMAILER_PASS
  }
})

function sendMail(subject, message, mail = mailResponsable){
  const mailOptions = {
    from: process.env.NODEMAILER_MAIL,
    to: mail,
    subject: subject,
    text: message,
    html: '<b>' + message + '</b>'
  };
  transporter.sendMail(mailOptions, (err, res) => {
    if(err) console.log(err)
  })
}

module.exports = { 
  sendMail 
}
